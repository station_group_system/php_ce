<?php


use Dotenv\Dotenv;
use Phalcon\Loader;

define('START_TIME', time());
define('ROOT_DIR', dirname(__DIR__));
define('APP_DIR', ROOT_DIR . '/app/');
define('XSSG_DIR', ROOT_DIR . '/xssg/');
define('MODULE_DIR', ROOT_DIR . '/module/');

#加载composer
require ROOT_DIR . '/vendor/autoload.php';
# 加载自动加载
$loader = new Loader();
$loader->registerNamespaces(
    [
        'xssg' => XSSG_DIR,
        'sys' => ROOT_DIR . '/sys/',
        'user' => MODULE_DIR . 'user',
    ]
);

$loader->register();
# 加载环境变量
$dotenv = new Dotenv(ROOT_DIR);
$dotenv->load();
include XSSG_DIR . '/function.php';


define('APP_DEBUG', true);
if (APP_DEBUG) {
    error_reporting(E_ALL);
    ini_set('log_errors', 'On');
    ini_set('error_log', ROOT_DIR . '/runtime/log/phplog.log');
}


define('INDEX_DIR', __DIR__);
define('RUNTIME_DIR', ROOT_DIR . '/runtime/');
define('RUN_UNIQID', substr(time(), -5) . uniqid());

define('MODULE', [
    '21' => 'article',
    '4' => 'company',
    '5' => 'sell'
]);


try {
    $application = new \xssg\Start();
    # 启动
    $config = require ROOT_DIR . '/config/config.php';
    $application->start($config);
    # 执行
    $Response = $application->handle();
    echo $Response->getContent();
    if (APP_DEBUG) {
        \xssg\tool\Trace::cache();
    }
} catch (Exception $e) {

    if (APP_DEBUG) {
        echo $e->getFile();
        echo "<br/>";
        echo $e->getLine();
        echo "<br/>";
        echo $e->getCode();
        echo "<br/>";
    }
    echo $e->getMessage();
    if (APP_DEBUG) {
        echo "<br/>";
        dump($e->getTrace());
    }


} catch (\Phalcon\Exception $e) {

    if (APP_DEBUG) {
        echo $e->getFile();
        echo "<br/>";
        echo $e->getLine();
        echo "<br/>";
        echo $e->getCode();
        echo "<br/>";
    }
    echo $e->getMessage();
    if (APP_DEBUG) {
        echo "<br/>";
        dump($e->getTrace());
    }
}

