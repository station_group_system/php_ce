<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登陆</title>
</head>
<body>
<h1>
    登陆
</h1>

<a href="{{ url('home/index/reg') }}">注册</a>

<!-- Specifying another method or attributes for the FORM tag -->
{{ form('user/user/login', 'method': 'post') }}
<label for='username'>Username:</label>

{{ text_field('username','用户名!') }}
<br>

<label for='password'>password:</label>
{{ password_field('password') }}
<br>


<br>
{{ hidden_field('to','value':'/home/user/index') }}
<br>
{{ submit_button('Search') }}



{{ endForm() }}

</body>
</html>