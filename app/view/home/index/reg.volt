<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>注册</title>
</head>
<body>
<h1>
    注册
</h1>

<a href="{{ url('home/index/login') }}">登陆</a>

<!-- Specifying another method or attributes for the FORM tag -->
{{ form('user/user/reg', 'method': 'post') }}
<label for='username'>Username:</label>

{{ text_field('username','用户名!') }}
<br>

<label for='password'>password:</label>
{{ password_field('password') }}
<br>


<label for='password2'>密码重复:</label>
{{ password_field('password2') }}
<br>
{{ hidden_field('to','value':'/home/user/index') }}
<br>
{{ submit_button('Search') }}



{{ endForm() }}

</body>
</html>