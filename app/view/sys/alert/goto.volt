<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
    <title>跳转提示页面</title>
    <style>
        * {
            box-sizing: border-box;
        }

        @media only screen and (min-width: 240px) {
            html {
                font-size: 75%;
            }
        }

        @media only screen and (min-width: 320px) {
            html {
                font-size: 87.5%;
            }
        }

        @media only screen and (min-width: 360px) {
            html {
                font-size: 100%;
            }
        }

        @media only screen and (min-width: 480px) {
            html {
                font-size: 112.5%;
            }
        }

        @media only screen and (min-width: 640px) {
            html {
                font-size: 150%;
            }
        }

        @media only screen and (min-width: 720px) {
            html {
                font-size: 175%;
            }
        }

        @media only screen and (min-width: 1024px) {
            html {
                font-size: 265%;
            }
        }

        body {
            background: #f3f3f3;
        }

        .load-box {
            width: 80%;
            position: absolute;
            top: 25%;
            left: 50%;
            margin-left: -40%;
            text-align: center;
        }

        .success, .error {
            display: inline-block;
            max-width: 80%;
            color: #666;
            font-size: 1.25rem;
            text-align: left;
            margin-bottom: 1rem;
            padding: 1.125rem 1rem 1.125rem 5rem;
            border-radius: 0.625rem;
            box-shadow: 0 0 0.625rem #ccc;
            background-color: #fff;
            background-image: url('data:image/svg+xml;%20charset=utf8,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%2050%2050%22%3E%3Ccircle%20cx=%2225%22%20cy=%2225%22%20r=%2225%22%20fill=%22#25ae88%22/%3E%3Cpath%20fill=%22none%22%20stroke=%22#fff%22%20stroke-width=%222%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22%20stroke-miterlimit=%2210%22%20d=%22M38%2015L22%2033l-10-8%22/%3E%3C/svg%3E');
            background-repeat: no-repeat;
            background-position: 1rem center;
            -webkit-background-size: auto 3rem;
            background-size: auto 3rem;
        }

        .error {
            background-image: url('data:image/svg+xml;%20charset=utf8,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%2050%2050%22%3E%3Ccircle%20cx=%2225%22%20cy=%2225%22%20r=%2225%22%20fill=%22#d75a4a%22/%3E%3Cpath%20fill=%22none%22%20stroke=%22#fff%22%20stroke-width=%222%22%20stroke-linecap=%22round%22%20stroke-miterlimit=%2210%22%20d=%22M16%2034l9-9%209-9M16%2016l9%209%209%209%22/%3E%3C/svg%3E')
        }

        .success span, .error span {
            display: block;
            margin-top: 0.125rem;
            font-size: 0.875rem;
            color: #777;
            font-weight: initial;
        }

        .goto-time {
            color: #d75a4a;
        }

        .btn-ok, .btn-no, .btn-stop {
            border: 0;
            outline: 0;
            background: #4378b7;
            border-radius: 0.313rem;
            color: #fff;
            padding: 0.625rem 1rem;
            font-size: 1.125rem;
            -webkit-transition: 0.5s;
            -moz-transition: 0.5s;
            text-decoration: none;
            margin: 0 0.625rem;
        }

        .btn-no {
            background: #888;
        }

        .btn-stop {
            background: #25ae88;

        }

        .btn-stop:active, .btn-stop:hover {
            background-color: #45bbb4;
        }

        .btn-ok:active, .btn-ok:hover {
            background-color: #2a5e9c;
        }

        .btn-no:active, .btn-no:hover {
            background-color: #555;
        }

        .load-box p {
            text-align: center;
            font-size: 1rem;
            line-height: 1.8;
            color: #666;
            margin: 0 0 1rem;
        }

        .load-box p span {
            display: inline-block;
            padding: 0 0.313rem;
            font-size: 1.25rem;
            font-weight: 700;
            -webkit-animation: text-color 1s -0.1s linear infinite;
            -moz-animation: text-color: 1s -0.1s linear infinite;
            -o-animation: text-color 1s -0.1s linear infinite;
            animation: text-color 1s -0.1s linear infinite
        }

        @-webkit-keyframes text-color {
            0% {
                opacity: 1;
            }
            50% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @-moz-keyframes text-color {
            0% {
                opacity: 1;
            }
            50% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @-o-keyframes text-color {
            0% {
                opacity: 1;
            }
            50% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @keyframes text-color {
            0% {
                opacity: 1;
            }
            50% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }


    </style>
</head>
<body>
<div class="load-box">
    {% if status %}
        <h1 class="success">{{ title }}<span>{{ description }}</span></h1>
    {% else %}
        <h1 class="error">{{ title }}<span>{{ description }}</span></h1>
    {% endif %}


    <p><span id="goto-time" class="goto-time">{{ wait }}</span>秒后页面将自动跳转</p>
    <button class="btn-ok" onclick="liji()">立即跳转</button>
    <button class="btn-stop" onclick="no()">停止跳转</button>
    <button class="btn-no">返回</button>

</div>
<script>
    function liji() {
        time = -1;
        zouni();
    }

    function no() {
        clearInterval(int223);
    }

    var time = document.getElementById('goto-time').innerText * 10;
    int223 = setInterval(function () {
        time--
        document.getElementById('goto-time').innerText = parseInt(time / 10);
        if (time <= 0) {
            zouni();
        }
    }, 100);

    function zouni() {
        clearInterval(int223);
        console.log('zouni');
        window.history.go(-1)
    }

</script>
</body>
</html>