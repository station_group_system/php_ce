<?php

namespace user\validation;

use sys\Validation\Validator\RequiredPro;
use user\validator\Password;
use user\validator\UserForbid as user_forbid;
use sys\Validation\Validator\ServerAction;

/**
 * 登录数据验证
 * @author Dongasai
 */
class Login extends \sys\Validation
{

//定义验证规则
    protected $rules = [
        'username' => [
            'required' => [
                'cancelOnFail' => true,
                "message" => "username"
            ],
        ],
        'password' => [
            'required' => [
                'cancelOnFail' => true,
                "message" => "password"
            ],
            'notempty' => [
                'cancelOnFail' => true,
                "message" => "password"
            ],
            'Validator' => [
                'cancelOnFail' => true,
                'name' => Password::class,
                "message" => "password"
            ],
        ]
    ];

    public function beforeValidation1($data)
    {


    }

    /**
     * 初始化的时候进行 验证规则解析
     */
    protected function initialize()
    {

        # 用户名,用户id必填一个
        $this->add_Validator('user_id', [
            'message' => 'RequiredPro',
            'name' => RequiredPro::class,
            'attrs' => [
                'username', 'user_id'
            ]
        ]);

        # 账户可用验证
        $this->add_Validator('user_id', [
            'name' => user_forbid::class,
            'message' => 'user_forbid'
        ]);


        return parent::initialize();
    }

}
