<?php

namespace xssg\sys;


class Controller extends \Phalcon\Mvc\Controller
{
    public $user_id = 0;

    public function initialize()
    {
        $this->user_id = $this->session->get('user_id', 0);
        $this->view->setVar('user_id', $this->user_id);

    }


}
 