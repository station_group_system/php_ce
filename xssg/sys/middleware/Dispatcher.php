<?php

namespace Xssg\Sys\Middleware;

class Dispatcher
{
    public function beforeNotFoundAction(\Phalcon\Events\Event $event, \Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $view = \Phalcon\Di::getDefault()->get('view');
        $view->setVar('controller', $dispatcher->getControllerName());
        $view->setVar('action', $dispatcher->getActionName());
        $view->setVar('module', $dispatcher->getModuleName());
        $dispatcher->forward(
            [
                'module' => 'home',
                "controller" => "index",
                "action" => "show404"
            ]
        );

        return false;
    }

}