<?php

namespace Xssg\Sys\Middleware;

use xssg\tool\Trace;

class View
{

    public function notFoundView($event, \Phalcon\Mvc\View $view)
    {
        if (APP_DEBUG) {
            throw new \Phalcon\Mvc\View\Exception('缺少模板文件:' . $view->getActiveRenderPath());
        }
        Trace::record('notFoundView', $view->getActiveRenderPath());
        //pre($view->getActiveRenderPath());
    }

    public function beforeRenderView()
    {
        //pre(func_get_args());
    }
}