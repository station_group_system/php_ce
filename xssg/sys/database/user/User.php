<?php

namespace xssg\sys\database\user;

use Phalcon\Db\Column as Column;
use Phalcon\Db\Index as Index;
use Phalcon\Db\Reference as Reference;

class User
{
    public $name = "user";

    public $columns = [

        "id" =>
            [
                "type" => Column::TYPE_INTEGER,
                "size" => 10,
                "unsigned" => true,
                "notNull" => true,
                "autoIncrement" => true,
                "first" => true,
                'primary' => true
            ]
        ,

        "username" =>
            [
                "type" => Column::TYPE_VARCHAR,
                "size" => 50,
                "unsigned" => true,
                "notNull" => true,
                "after" => "id"
            ]
        ,

        "password" =>
            [
                "type" => Column::TYPE_VARCHAR,
                "size" => 100,
                "unsigned" => true,
                "notNull" => true,
                "after" => "username"
            ]

    ];
    public $indexes = [
        'PRIMARY' => [
            'name' => 'PRIMARY',
            'cloumns' => ['id'],
            'type' => 'PRIMARY'
        ],
        'username' => [
            'name' => 'username',
            'cloumns' => ['username'],
            'type' => 'UNIQUE'
        ],
    ];
    public $references = [];
    public $options = [];


}