<?php

namespace Xssg\Sys;

class Volt
{
    public function start($view, $di)
    {

        $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
        // Set some options here
        $volt->setOptions([
            "compileAlways" => APP_DEBUG
        ]);
        $compiler = $volt->getCompiler();

        $compiler->addFunction(
            "dumpall", function ($resolvedArgs, $exprArgs) {
            return "dumppro($this->view->getParamsToView())";
        });

        $compiler->addFunction(
            "dumpallname", function ($resolvedArgs, $exprArgs) {
            return "dumppro(array_keys ($this->view->getParamsToView()))";
        });

        //substr
        $compiler->addFunction(
            "substr", function ($resolvedArgs, $exprArgs) {

            return "mb_substr(" . $resolvedArgs . ")";
        });

        $compiler->addFunction(
            "trace", function ($resolvedArgs, $exprArgs) {
            return "\xssg\\tool\\Trace::tohtml();";
        });
        return $volt;
    }

}