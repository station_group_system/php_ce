<?php

namespace Xssg\home\controller;

use xssg\sys\Controller;

/**
 * 用户控制器(登陆注册页面!)
 * Class user
 * @package Xssg\home\controller
 */
class User extends Controller
{
    public function initialize()
    {
        parent::initialize();
        if ($this->user_id) {
            $this->response->redirect('user/index/index');
        }
    }


    public function login()
    {

    }

    public function reg()
    {

    }


}