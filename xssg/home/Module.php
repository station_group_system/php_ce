<?php

namespace Xssg\Home;

use Phalcon\DiInterface;


class Module implements \Phalcon\Mvc\ModuleDefinitionInterface
{
    /**
     * 注册模块的自动加载
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $dependencyInjector = null)
    {

    }

    /**
     * 注册模块的服务依赖注入
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $dependencyInjector)
    {
        $config = $dependencyInjector->get("config");
        $view = $dependencyInjector->get("view");
        $view->setViewsDir($config->view->viewsdir . "/home/");
        $dependencyInjector->set("view", $view);
        $dispatcher = $dependencyInjector->get('dispatcher');
        $dispatcher->setDefaultNamespace("\xssg\home\controller\\");
        $dependencyInjector->set('dispatcher', $dispatcher);

    }

}