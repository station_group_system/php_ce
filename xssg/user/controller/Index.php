<?php

namespace Xssg\user\controller;

class Index extends \xssg\sys\Controller
{

    public function index()
    {
        return $this->view->render('index', 'home');
    }

}