<?php
/**
 * Created by PhpStorm.
 * user: saisai
 * Date: 19-1-18
 * Time: 上午11:53
 */

function pre()
{
    if (APP_DEBUG) {
        $degbug = debug_backtrace();
        echo "file : " . $degbug[0]['file'] .
            ' <br> line: ' . $degbug[0]['line'] .
            ' <br>  func:' . $degbug[0]['function'];

        //dump($degbug);
        foreach (func_get_args() as $value) {
            dump($value);
        }
    }
    exit;

}

function pr()
{
    if (APP_DEBUG) {
        $degbug = debug_backtrace();
        //dump($degbug);
        foreach (func_get_args() as $value) {
            echo "file : " . $degbug[1]['file'] . ' <br> line: ' . $degbug[1]['line'] .
                ' <br>  func:' . $degbug[1]['function'];
            dump($value);
        }
    }
}

/**
 * 获取类名
 * @param $classname
 * @return bool|int|string
 */
function get_class_name($classname)
{
    if ($pos = strrpos($classname, '\\')) return substr($classname, $pos + 1);

    return $pos;
}