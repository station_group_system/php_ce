<?php

namespace xssg\tool;

class Trace
{

    private static $number = [];

    private static $record = [];

    /**
     * 增加一条记录
     * @param key
     * @param data
     */
    public static function record(string $key, $data)
    {
        if (isset (self::$record[$key])) {
            self::$record[$key][] = $data;
        } else {
            self::$record[$key] = [];
            self::$record[$key][] = $data;
        }

    }

    /**
     * 自增
     * @param key
     */
    public static function inc(string $key)
    {
        if (isset (self::$number[$key])) {
            self::$number[$key] = self::$number[$key] + 1;
        } else {
            self::$number[$key] = 1;
        }
    }

    /**
     * 自减
     * @param key
     */
    public static function rec(string $key)
    {
        if (isset (self::$number[$key])) {
            self::$number[$key] = self::$number[$key] - 1;
        } else {
            self::$number[$key] = -1;
        }
    }

    public static function cache()
    {
        \Phalcon\Di\FactoryDefault::getDefault()->getShared("cache")->save(RUN_UNIQID, self::getData());
    }

    private static function getData()
    {
        return [
            "number" => self::$number,
            "record" => self::$record
        ];
    }

    public static function tohtml()
    {

        $url = \Phalcon\Di\FactoryDefault::getDefault()->getShared("url");
        return "<a style=\"position:fixed;right:0;bottom:0;z-index: 999;\" href=\"" . $url->get("home/trace/index", [
                "id" => RUN_UNIQID
            ]) . "\">Trace</a>";
    }
}