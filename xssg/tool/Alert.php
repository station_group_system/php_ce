<?php

namespace xssg\tool;

use Phalcon\Di\Injectable;
use Phalcon\Mvc\View\Engine\Volt;

class Alert extends Injectable
{

    /**
     * 成功的提示
     * @param $title
     * @param $description
     * @param int $wait
     */
    public static function error($title, $description, $wait = 10)
    {

        $view = clone \Phalcon\Di\FactoryDefault::getDefault()->get('view');
        $view->start();
        $view->setViewsDir(
            \Phalcon\Di\FactoryDefault::getDefault()
                ->get('config')->view->viewsdir . "/sys/"
        );
        $view->setVars([
            'title' => $title,
            'description' => $description,
            'wait' => $wait,
            'status' => 0
        ]);

        $view->render('alert', 'goto');
        $view->finish();
        return $view->getContent();

    }

    /**
     * 失败的提示
     * @param $title
     * @param $description
     * @param int $wait
     */
    public static function success($title, $description, $wait = 5)
    {

        $view = clone \Phalcon\Di\FactoryDefault::getDefault()->get('view');
        $view->start();
        $view->setViewsDir(
            \Phalcon\Di\FactoryDefault::getDefault()
                ->get('config')->view->viewsdir . "/sys/"
        );
        $view->setVars([
            'title' => $title,
            'description' => $description,
            'wait' => $wait,
            'status' => 1
        ]);


        $view->render('alert', 'goto');

        $view->finish();

        return $view->getContent();
    }

    /**
     * 状态码
     * @param $status
     * @param $title
     * @param $description
     * @param int $wait
     */
    public static function status(string $status, $title, $description, $wait = 100)
    {
        $view = clone \Phalcon\Di\FactoryDefault::getDefault()->get('view');
        $response = \Phalcon\Di\FactoryDefault::getDefault()->get('response');
        $response->setStatusCode($status);
        $view->setVars([
            'title' => $title,
            'description' => $description,
            'wait' => $wait,
            'status' => 0
        ]);

        $tmp = VIEW_DIR . 'alert/' . $status . '.volt';

        if (is_file($tmp)) {
            $view->pick('alert/' . $status);
            $view->render('alert', '404');
        } else {
            //$this->view->render('alert','status');
            $view->pick('alert/status');
            $view->render('alert', 'status');
        }

        $re = $view->getContent();
        $view->finish();
        return $re;
    }

}