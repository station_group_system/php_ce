<?php

namespace sys\Validation\Validator;

/**
 * 减弱版本的正则验证,
 * Class RegexSlack
 * @package sys\Validation\Validator
 */
class RegexSlack extends \sys\Validation\Validator
{

    public function validate(\Phalcon\Validation $validation, $attribute)
    {

        $value = $validation->getValue($attribute);
        $pattern = $this->getOption("pattern");
        if (preg_match($pattern, $value)) {
            # 通过
            return true;
        } else {
            $this->type = 'RegexSlack';
            return $this->appendMessage($validation, $attribute);
            # 不通过
        }
    }
}